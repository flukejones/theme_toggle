#!/bin/bash

export RUSTFLAGS='-Clink-arg=-Wl,-z,relro,-z,now,-s'
cargo build --release
cargo install --path ${PWD} --force --root ~/.local
install -D app/theme-toggle.desktop $HOME/.local/share/applications/theme-toggle.desktop
install -D app/daynight.svg $HOME/.local/share/icons/hicolor/scalable/apps/daynight.svg

echo ""
echo "Don't forget to add '$HOME/.local/bin' to your path"