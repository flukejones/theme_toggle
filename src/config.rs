use date_time::time_tuple::TimeTuple;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};

const CONF_DIR: &str = "/.config/theme_toggle.json";
const DEFAULT: &str = r#"{
  "current": "light",
  "time": {
    "daylight_savings": true,
    "light": [8, 0, 0],
    "dark": [20, 30, 0]
  },
  "globals": {
        "gtk": {
            "dark": "'Adwaita-dark'",
            "light": "'Adwaita'"
        },
        "sourceview": {
            "dark": "'builder-dark'",
            "light": "'builder'"
        },
        "shell": {
            "dark": "",
            "light": ""
        }
    },
    "commands": {
        "dark": [["dconf", "write", "/org/gnome/builder/night-mode", "true"],
                 ["dconf", "write", "/org/gnome/terminal/legacy/theme-variant", "'dark'"],
                 ["sed", "-i", "-E", "s|(:color) .+|\\1 torte|g", "/home/luke/.vimrc"]],
        "light": [["dconf", "write", "/org/gnome/builder/night-mode", "false"],
                  ["dconf", "write", "/org/gnome/terminal/legacy/theme-variant", "'light'"],
                  ["sed", "-i", "-E", "s|(:color) .+|\\1 morning|g", "/home/luke/.vimrc"]],
        "sourceview": [["dconf", "write", "/org/gnome/builder/editor/style-scheme-name"],
                       ["dconf", "write", "/org/gnome/gedit/preferences/editor/scheme"]],
        "gtk": [["dconf", "write", "/org/gnome/desktop/interface/gtk-theme"]],
        "shell": [["dconf", "write", "/org/gnome/shell/extensions/user-theme/name"]],
        "misc": []
    }
}"#;

#[derive(Serialize, Deserialize, Debug)]
pub struct Time {
    daylight_savings: bool,
    light: [i32; 3],
    dark: [i32; 3],
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ToggleList {
    current: String,
    time: Time,
    globals: HashMap<String, HashMap<String, String>>,
    commands: HashMap<String, Vec<Vec<String>>>,
}

impl ToggleList {
    #[inline]
    pub fn new() -> ToggleList {
        ToggleList {
            current: String::new(),
            time: Time {
                daylight_savings: false,
                light: [0; 3],
                dark: [0; 3],
            },
            globals: HashMap::new(),
            commands: HashMap::new(),
        }
    }

    pub fn read(mut self) -> Self {
        if let Ok(home) = env::var("HOME") {
            let conf_file = home + CONF_DIR;
            let mut file = OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(&conf_file)
                .expect("config file error");
            let mut buf = String::new();
            if let Ok(l) = file.read_to_string(&mut buf) {
                if l == 0 {
                    file.write_all(DEFAULT.as_bytes())
                        .expect("Writing default config failed");
                    self = serde_json::from_str(DEFAULT).unwrap();
                } else {
                    self = serde_json::from_str(&buf).unwrap();
                }
            }
        } else {
            panic!("Configuration file error");
        }
        self
    }

    pub fn write(&self) {
        match env::var("HOME") {
            Ok(home) => {
                let mut file = File::create(home + CONF_DIR).expect("Couldn't overwrite config");
                let json_str =
                    serde_json::to_string_pretty(self).expect("Parse config to JSON failed");
                file.write_all(json_str.as_bytes())
                    .expect("Saving config failed");
            }
            Err(e) => panic!(format!("Failed to get $HOME: {:?}", e)),
        }
    }

    pub fn get_current(&self) -> &str {
        &self.current
    }

    pub fn set_current(&mut self, cur: &str) {
        self.current = cur.to_string();
    }

    pub fn globals(&self) -> &HashMap<String, HashMap<String, String>> {
        &self.globals
    }

    pub fn commands(&self) -> &HashMap<String, Vec<Vec<String>>> {
        &self.commands
    }

    pub fn get_light_time(&self) -> TimeTuple {
        let mut h = self.time.light[0];
        let m = self.time.light[1];
        let s = self.time.light[2];
        if self.time.daylight_savings {
            h += 1;
        }
        TimeTuple::new(h, m, s)
    }

    pub fn get_dark_time(&self) -> TimeTuple {
        let mut h = self.time.light[0];
        let m = self.time.light[1];
        let s = self.time.light[2];
        if self.time.daylight_savings {
            h += 1;
        }
        TimeTuple::new(h, m, s)
    }
}
