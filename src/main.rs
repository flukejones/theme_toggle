use date_time::time_tuple::TimeTuple;
use std::{
    env,
    fs::remove_file,
    io::{Read, Write},
    os::unix::net::{UnixListener, UnixStream},
    path::Path,
    process::Command,
    time::Duration,
};

mod config;
use crate::config::ToggleList;

pub static SOCKET_PATH: &'static str = "/tmp/theme_toggle_sock";

fn main() {
    let socket = Path::new(SOCKET_PATH);
    let mut daemon = false;

    // Always need to read the settings
    let mut config = ToggleList::new().read();
    // Only need the first arg. Anything at all after this is junk
    if let Some(arg) = env::args().nth(1) {
        match arg.as_str() {
            "daemon" => {
                println!("Running as daemon");
                daemon = true;
            }
            a => {
                if let Ok(mut stream) = UnixStream::connect(socket) {
                    stream.write_all(a.as_bytes()).unwrap();
                }
            }
        }
    } else {
        do_toggle(&mut config);
        run_all(&config);
        config.write();
    };

    if daemon {
        let mut stream_buf = String::new();

        if socket.exists() {
            remove_file(&socket).unwrap();
        }
        // Bind to socket
        let stream = match UnixListener::bind(socket) {
            Ok(stream) => stream,
            Err(e) => panic!("failed to bind socket: {:?}", e),
        };

        loop {
            for st in stream.incoming() {
                if let Ok(mut m) = st {
                    m.set_read_timeout(Some(Duration::new(1, 0))).unwrap();
                    m.read_to_string(&mut stream_buf).unwrap();
                    println!("Recv: {:?}", stream_buf);
                    match stream_buf.as_str() {
                        "toggle" => {
                            do_toggle(&mut config);
                            run_all(&config);
                        }
                        _ => println!("Command is unused: {}", stream_buf),
                    }
                    stream_buf.clear();
                };
            }
            let time_now = TimeTuple::now();
            // Day time?
            if time_now > config.get_light_time()
                && time_now < config.get_dark_time()
                && config.get_current() == "dark"
            {
                println!("Switching to light theme");
                config.set_current("light");
            } else if config.get_current() == "light" {
                println!("Switching to dark theme");
                config.set_current("dark");
            }
        }
    }
}

fn run_all(config: &ToggleList) {
    for (tog, com) in &mut config.commands().iter() {
        if tog == config.get_current() {
            for list in com {
                exec_command(list, None);
            }
        } else if let Some(global) = config.globals().get(tog) {
            if let Some(opt) = global.get(config.get_current()) {
                for list in com.iter() {
                    exec_command(&list, Some(opt));
                }
            } else {
                println!(
                    "No matching toggle element for {} = {}",
                    tog,
                    config.get_current()
                );
            }
        } else if tog == "misc" {
            for list in com.iter() {
                exec_command(list, None);
            }
        } else if tog != "light" && tog != "dark" {
            println!("Command list {:?} is faulty", com);
        }
    }
}

fn exec_command(list: &[String], opt: Option<&str>) {
    let mut com = Command::new(&list[0]);
    com.args(list[1..].iter());
    if let Some(o) = opt {
        com.arg(o);
    }
    com.output()
        .unwrap_or_else(|_| panic!("Comand list failed: {:?}", list));
}

fn do_toggle(config: &mut ToggleList) {
    match config.get_current() {
        "light" => config.set_current("dark"),
        "dark" => config.set_current("light"),
        &_ => config.set_current("dark"),
    };
}
