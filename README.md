[![Build Status](https://drone.systemscoder.nz/api/badges/luke/theme-toggle/status.svg)](https://drone.systemscoder.nz/luke/theme-toggle)

# Theme Toggle

Initially I'd written this as a bash script, but then it became a pain to manage with
more complexity and slowness than I wanted. So I made it simply a little more complex. Maybe.

It was a good excuse to play with a few things in Rust like running external commands, and
to figure out if I could turn it in to a daemon (there's no compelling reason to do so, it's
a "Just coz").

The configuration is in JSON, and can run arbitrary commands. So you could set this up
to wipe your harddrive when you switch to night mode so you lose all ability to sleep that night.

## Running

The app will iterate through the commands list in the JSON config. Command array names are
run as matches:

1. If name is opposite of current variant (dark/light), run commands in array,
2. If name has matching global, append global variant to command,
3. If name is not in the above, run commands in array.

Option #3 means that you can run a set of arbitrary commands every time the app is run.

### Examples

An example of extra `light`/`dark` commands using vim as follows:

```
"dark": [["sed", "-i", "-E", "s|(:color) .+|\\1 torte|1", "/home/luke/.vimrc"]]
"light": [["sed", "-i", "-E", "s|(:color) .+|\\1 morning|1", "/home/luke/.vimrc"]]
```
As you can see, the full command is required to be broken in to its components: command
followed by each of its args in quotes.

Another example is using `ddc` or `xrandr` to control screen brightness:

```
"dark": [
    ["xrandr", "--output", "DP-0", "--brightness", "0.55"],
    ["ddcutil", "setvcp", "10", "0", "-d", "1"]
],
"light": [
    ["xrandr", "--output", "DP-0", "--brightness", "1.0"],
    ["ddcutil", "setvcp", "10", "50", "-d", "1"]
],
```

*Note:* Gnome night-light seems to reset the xrandr brightness back to 1.0, you may need to turn it off.

## Default Config

This is what gets written to `~/.config/theme_toggle` by default.

``` json
{
  "current": "light",
  "time": {
    "daylight_savings": true,
    "light": [8, 0, 0],
    "dark": [20, 30, 0]
  },
  "globals": {
        "gtk": {
            "dark": "'Adwaita-dark'",
            "light": "'Adwaita'"
        },
        "sourceview": {
            "dark": "'builder-dark'",
            "light": "'builder'"
        },
        "shell": {
            "dark": "",
            "light": ""
        }
    },
    "commands": {
        "dark": [["dconf", "write", "/org/gnome/builder/night-mode", "true"]],
                 ["dconf", "write", "/org/gnome/terminal/legacy/theme-variant", "'dark'"],
                 ["sed", "-i", "-E", "s|(:color) .+|\\1 torte|g", "/home/luke/.vimrc"]],
        "light": [["dconf", "write", "/org/gnome/builder/night-mode", "false"],
                  ["dconf", "write", "/org/gnome/terminal/legacy/theme-variant", "'light'"],
                  ["sed", "-i", "-E", "s|(:color) .+|\\1 morning|g", "/home/luke/.vimrc"]],
        "sourceview": [["dconf", "write", "/org/gnome/builder/editor/style-scheme-name"],
                       ["dconf", "write", "/org/gnome/gedit/preferences/editor/scheme"]],
        "gtk": [["dconf", "write", "/org/gnome/desktop/interface/gtk-theme"]],
        "shell": [["dconf", "write", "/org/gnome/shell/extensions/user-theme/name"]],
        "misc": []
    }
}
```

# License

App is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

App icon from [here](https://image.flaticon.com/icons/svg/215/215693.svg), unsure of license.
